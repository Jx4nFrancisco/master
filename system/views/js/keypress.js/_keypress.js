/* Reglas globales para validacion de parte del cliente */
/*
$(function() {
  $('html').on('click', '#enviar', function() {
    //alert('holaaaaaaaaaaaa');
    $('#form-signin').validate({
      rules: {
        usuario: {
          required: true,
          minlength: 2
        }
      },
      messages: {
        usuario: {
          required: 'campo necesario',
          minlength: 'solo dos caracteres'
        }
      }
    });
  });
});
*/
/* Reglas al presionar teclas */
/*---------- 1 formulario ingresar -------------------------------------------*/
/* --------- input usuario -----------------------------*/
$('html').on('keyup', '#usuario', function(e) {
  tecla = e.keyCode;
  //alert("presionaste =" + tecla);
  if (tecla == 13) {
    /* Dar foco a input contraseña si el input usuario valida un minimo de caracteres y si no esta vacio */
    if ($('#usuario').val().length > 4) {
      $('#contraseña').focus();
    };
  };
});
/* modificacion para visualizar de forma correcta el formulario en index*/
$('html').on('click', '#usuario', function() {
  var mediaquery = window.matchMedia("(max-width: 600px)");
  if (mediaquery.matches) {
    $('#sp').html('<br><br><br><br>');
    $('#text-footer-1').html('Reglamento');
  };
});
/* --------- input contraseña -----------------------------*/
/* Impido que el usuario escriba contraseña antes que usuario*/
$('html').on('keyup', '#contraseña', function(e) {
  if ($('#usuario').val() == '') {
    $('#usuario').focus();
    $('#contraseña').val('');
    $('#contraseña').blur();
  };

  tecla = e.keyCode;
  if (tecla == 13) {
    /* Si ingresamos contraseña damos click a btn ingresar con enter */
    if ($('#contraseña').val().length >= 6) {
      $('#enviar').click();
    };
  };
});
/* --------- boton enviar -----------------------------*/
$('html').on('click', '#enviar', function() {
  /* Si presiono aceptar y los input estan vacios
   * Doy foco a input usuario
   * Si usuario no esta vacio le doy foco a input contraseña
   */
  if ($('#usuario').val() == '') {
    $('#usuario').focus();
    $('#contraseña').val('');
  } else {
    $('#contraseña').focus();
  };
});
/*---------- 2 formulario recuperar contraseña -----------------------------*/
$('html').on('click', '#recuperar', function() {
  if ($('#nombre-usuario').val() == '') {
    $('#nombre-usuario').focus();
  };

});
/*
 * Si el usuario no escribio su usuario en input le hablitamos click
 * con keyup sobre enter
 */
$('html').on('keyup', '#nombre-usuario', function(e) {
  tecla = e.keyCode;
  if (tecla == 13) {
    /* Si ingresamos usuario damos click a btn ingresar con enter */
    if ($('#nombre-usuario').val().length >= 5) {
      $('#recuperar').click();
    };
  };
});
/*---------- 3 formulario ingresar codigo ------------------------------------*/
$('html').on('click', '#continuar', function() {
  if ($('#codigo').val() == '') {
    $('#codigo').focus();
  };

});
/*
 * Si el usuario no escribio su usuario en input le hablitamos click
 * con keyup sobre enter
 */
$('html').on('keyup', '#codigo', function(e) {
  tecla = e.keyCode;
  if (tecla == 13) {
    /* Si ingresamos usuario damos click a btn ingresar con enter */
    if ($('#codigo').val().length >= 7) {
      $('#recuperar').attr('disabled');
      //alert('press continuar');
      $(".card-content").load('./system/views/ingreso/formularios/_contraseña_nueva.html');
      $('#continuar').click();
    };
  };
});
/*---------- 4 formulario contraseña nueva -------------------------------------------*/
/* --------- input Contraseña -----------------------------*/
$('html').on('keyup', '#newContraseña', function(e) {
  tecla = e.keyCode;
  //alert("presionaste =" + tecla);
  if (tecla == 13) {
    /* Dar foco a input valContraseña si el input usuario valida un minimo de caracteres */
    if ($('#newContraseña').val().length > 7) {
      $('#valContraseña').focus();
    };
  };
});
/* --------- input valContraseña -----------------------------*/
/* Impido que el usuario escriba valContraseña antes que contraseña*/
$('html').on('keyup', '#valContraseña', function(e) {
  if ($('#newContraseña').val() == '') {
    $('#newContraseña').focus();
    $('#valContraseña').val('');
    $('#valContraseña').blur();
  };

  tecla = e.keyCode;
  if (tecla == 13) {
    /* Si ingresamos contraseña damos click a btn ingresar con enter */
    if ($('#valContraseña').val().length > 7) {
      $('#he-terminado').click();
    };
  };
});
/* modificacion para visualizar de forma correcta el formulario en index*/
$('html').on('click', '#usuario', function() {
  var mediaquery = window.matchMedia("(max-width: 600px)");
  if (mediaquery.matches) {
    $('#sp').html('<br><br><br><br>');
    $('#text-footer-1').html('Reglamento');
  };
});
/* --------- boton he-terminado -----------------------------*/
$('html').on('click', '#he-terminado', function() {
  /* Si presiono aceptar y los input estan vacios
   * Doy foco a input usuario
   * Si usuario no esta vacio le doy foco a input contraseña
   */
  if ($('#newContraseña').val() == '') {
    $('#newContraseña').focus();
    $('#valContraseña').val('');
  } else {
    $('#valContraseña').focus();
  };
});
