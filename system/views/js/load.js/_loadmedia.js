/*----------- Detectar dinamicamente el cambio de mi dom para aplicar eventos ----------------------------------*/

$(document).ready(function() {
  /*
   * definicion de variables para la media de media a evaluar para eventos
   * funciones que detectara el cambio progresivo del media del DOM durante el sistema
   */
  var mediaquery600 = window.matchMedia("(max-width: 600px)");
  /*---- funcion media 600--------------------------------------------*/
  function handleOrientationChange600(mediaquery600) {
    /*--diferente a 600--*/
    var media600 = $(window).width() != 600;
    /*--Mayor a 600--*/
    var menor600 = $(window).width() <= 600;
    /*--Menor a 600--*/
    var mayor600 = $(window).width() >= 601;
    /*---- switch para evaluar opcion de media ---------------------*/
    switch (media600) {

      case menor600:
        /*alert('opcion 600 1');*/
        $('#copyright').hide();
        $('.page-footer').css('background-color', 'white');
        break;

      case mayor600:
        /*alert('opcion 600 2');*/
        $('#copyright').show();
        break;

      default:

    }
  }
  mediaquery600.addListener(handleOrientationChange600);

  var mediaquery990 = window.matchMedia("(max-width: 990px)");
  /*---- funcion media 990--------------------------------------------*/
  function handleOrientationChange990(mediaquery990) {
    /*--diferente a 990--*/
    var media990 = $(window).width() != 990;
    /*--Mayor a 990--*/
    var menor990 = $(window).width() <= 990;
    /*--Menor a 990--*/
    var mayor990 = $(window).width() >= 991;
    /*---- switch para evaluar opcion de media ---------------------*/
    switch (media990) {

      case menor990:
        $(document).ready(function() {
          $('.button-collapse').sideNav({
            menuWidth: 300,
            edge: 'left',
            closeOnClick: true,
            draggable: true,
            onOpen: function(el) { /*Do Stuff*/ },
            onClosed: function(el) { /*Do Stuff*/ }
          });
        });
        break;

      case mayor990:
        var data = $('body').attr('datatype');
        var inicio = 'inicio';
        if (data != inicio) {
          $('header').addClass('on');
          $('main').addClass('on');
          $('footer').addClass('on');
          $('#copyright').show();
        }
        break;

      default:

    }
  }
  mediaquery990.addListener(handleOrientationChange990);

  var mediaqueryWH = window.matchMedia("(max-height: 601px)");;

  function handleOrientationChange601(mediaqueryWH) {
    /*--Menor height 600--*/
    var menor600 = $(window).height() <= 601;
    if (menor600) {
      $('footer').hide();
    } else {
      $('footer').show();
    }
  }
  mediaqueryWH.addListener(handleOrientationChange601);

});
