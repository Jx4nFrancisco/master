/* -------------1 Ajax y acciones que realizara mi SignIn---------------- */
$("html").on("click", "#enviar", function(e) {
  e.preventDefault();
  /* obtengo datos ingresado */
  var usuario = $("#usuario").val();
  var contraseña = $("#contraseña").val();
  var datos = {
    usuario: usuario,
    contraseña: contraseña
  };
  var controller = "core/controlador_base.php";
  //var url = "system/controller/_general/_signin.controller.php";
  var html = "system/views/load/load_milla.html"; /* alert("usuario : "+usuario+" contraseña : "+contraseña); */ /* dato devuleto por php pasado json_encode */
  $.ajax({
      url: controller,
      type: "POST",
      dataType: "json",
      data: datos,
      /* Progreso de carga de mi ajax */
      xhr: function() {
        var xhr = new window.XMLHttpRequest();
        var progress = $('#progress');
        var determinate = $('.determinate');
        xhr.upload.addEventListener('progress', function(evt) {
          if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            /*-- console.log(percentComplete * 100 + '%'); --*/
            /* progress
              .removeClass('hide')
              .addClass('determinate');
            determinate
              .css('width', percentComplete * 100 + '%');
            //width: percentComplete * 100 + '%'
            //);
            /* Ya cargado se oculta barra de progreso */
            if (percentComplete === 1) {
              //$('.progress').addClass('hide');
            }
          }
        }, false);
        xhr.addEventListener('progress', function(evt) {
          if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            /*-- console.log(percentComplete * 100 + '%'); --*/
            /*$('.progress').css({
              width: percentComplete * 100 + '%'
            });*/
            /*
            progress
              .removeClass('hide')
              .addClass('determinate');
            determinate
              .css('width', percentComplete * 100 + '%');
              */
          }
        }, false);
        return xhr;
      }
    })
    .done(function(result) {
      /*prueba de funcionamiento
      console.log("peticion retorno existosamente");
      console.log("usuario: " + result[0].usuario);
      console.log("contraseña: " + result[0].contraseña);
      */
      /* Validacion usuario existente y redireccionamiento; variables de ingreso */
      var i1 = "Supervisor",
        i2 = "Secretario Tecnico",
        i3 = "Gerencia",
        i4 = "Recursos Humanos",
        i5 = "Administración y Finanza",
        i6 = "Adquisición y Bodega",
        i7 = "Adquisición y Logistica",
        i8 = "Operaciones y Proyectos";
      var departamento = result[0].departamento;
      var cargo = result[0].cargo;
      /*--DataType de body cambia '' --*/
      var bodyType = 'Activo';
      //alert(departamento);
      //alert(cargo);
      if (cargo != null) {
        $('body').attr('dataType', bodyType);
        switch (cargo) {
          case i1:
            $('header').load('./system/views/usuarios/supervisor/header.html');
            $('main').load('./system/views/usuarios/supervisor/contenido.html');
            break;
          case i2:
            $('header').load('./system/views/usuarios/gerente/header.html');
            $('main').load('./system/views/usuarios/gerente/contenido.html');
            break;
            /*default:*/
        }
      } else if (departamento != null) {
        $('body').attr('dataType', bodyType);
        switch (departamento) {

          case i3:
            $('header').load('./system/views/usuarios/gerente/header.html');
            $('main').load('./system/views/usuarios/gerente/contenido.html');
            break;
          case i4:
            $('header').load('./system/views/usuarios/rrhh/header.html');
            $('main').load('./system/views/usuarios/rrhh/contenido.html');
            break;
          case i5:
            $('header').load('./system/views/usuarios/administracion/header.html');
            $('main').load('./system/views/usuarios/administracion/contenido.html');
            break;
          case i6:
            $('header').load('./system/views/usuarios/Adquisicion_bodega/header.html');
            $('main').load('./system/views/usuarios/Adquisicion_bodega/contenido.html');
            break;
          case i7:
            $('header').load('./system/views/usuarios/finanza/header.html');
            $('main').load('./system/views/usuarios/finanza/contenido.html');
            break;
            /*default:*/
        }
      }
      /*try {
        if (result[0].usuario == usuario && result[0].contraseña == contraseña) {
          $(".card-content").load(resultContenido.url.pag2);
        }
      } catch (e) {
        //console.log(result);
      };*/
    })
    .fail(function(result) {
      /*console.log("peticion retorno error");*/
      /* damos el foco a nuestros input */
      if (usuario == '' && contraseña == '') {
        $('#usuario').focus();
      } else if (contraseña == '') {
        $('#contraseña').focus();
      }else {
        var progress = $('#progress');
        var resultSet = 'Verifica tu usuario o clave';
        progress
          .removeClass('hide')
          .removeClass('progress')
          .addClass('center-align')
          .html(resultSet);
      }
    });
});
