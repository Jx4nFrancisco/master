/* ------------- 2 Ajax y acciones que realizara mi recuperar password -------*/
$("html").on("click", "#recuperar", function(e) {
  e.preventDefault();
  /* obtengo datos ingresado */
  var usuario = $("#nombre-usuario").val();
  var dato = {
    recuperarUsuario: usuario
  }

  var controller = 'core/controlador_base.php' /*url*/
  $.ajax({
    url: controller,
    type: 'POST',
    dataType: 'json',
    data: dato
  })
  .done(function(result) {
    console.log("success"+result[0].usuario+"correo"+result[0].trabajadorObra+"correo"+result[0].trabatrabajadorAdmin);
    usuariodb = result[0].usuario;
    if (usuariodb == usuario) {
      //alert(usuario);
      $(".card-content").load('./system/views/ingreso/formularios/_ingresa_codigo.html');
    }
  })
  .fail(function(result) {
    console.log("error");
    /* damos el foco a nuestros input */
    if (usuario == '') {
      $('#nombre-usuario').focus();
      $('#nombre-usuario').val();
    }else {
      var progress = $('#progress');
      //var result = 'Verifica tu usuario';
      progress
        .removeClass('hide')
        .removeClass('progress')
        .addClass('center-align')
        .html(result);

    }
  })
  .always(function(result) {
    /*console.log("complete");*/
  });

});
