  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>

    <!-- Aqui es donde cargamos el sidebar -->
    <ul id="slide-out" class="side-nav">
    <li><div class="user-view">
      <div class="background">
        <img src="images/office.png">
      </div>
      <a href="#!user"><img class="circle" src="images/office.png"></a>
      <a href="#!name"><span class="white-text name">Empleado Ejemplo</span></a>
      <a href="#!email"><span class="white-text email">ejemplo@ejemplo.com</span></a>
    </div></li>
      <li><a href="#!"><i class="material-icons">cloud</i>First Link With Icon</a></li>
      <li><a href="#!">Second Link</a></li>
      <li><div class="divider"></div></li>
      <li><a class="subheader">Subheader</a></li>
      <li><a class="waves-effect" href="#!">Third Link With Waves</a></li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>

    <!-- Page Layout here -->
    <div class="row">

    <!-- Este sera el mantenedor principal - registro de usuarios nuevos -->
    <div class="col s4">
    <div class="card small">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="images/office.png">
    </div>
    <div class="card-content">
      <span class="card-title activator grey-text text-darken-4">Usuarios<i class="material-icons right">more_vert</i></span>
      <p><a href="#"></a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4">Agregar Nuevo Usuario<i class="material-icons right">close</i></span>
      
  <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <input placeholder="Placeholder" id="first_name" type="text" class="validate">
          <label for="first_name">Nombres</label>
        </div>

        <div class="input-field col s6">
          <input id="last_name" type="text" class="validate">
          <label for="last_name">Apellidos</label>
        </div>
      </div>

       <div class="row">
        <div class="input-field col s12">
          <input id="password" type="password" class="validate">
          <label for="password">Nombre de Usuario</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s12">
          <input id="password" type="password" class="validate">
          <label for="password">Contraseña</label>
        </div>
      </div>

      <div class="row">
        <div class="col s12">
          <div class="input-field inline">
            <input id="email" type="email" class="validate">
            <label for="email" data-error="wrong" data-success="right">Email</label>
          </div>
        </div>
      </div>
    </form>
  </div> <!-- Cerramos formulario -->

    </div>
    </div>
    </div> <!-- Cerramos usuarios -->

    <!-- Este sera el mantenedor donde se asignara el cargo al usuario ya ingresado al sistema -->
    <div class="col s4">
    <div class="card small">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="images/office.png">
    </div>
    <div class="card-content">
      <span class="card-title activator grey-text text-darken-4">Cargos<i class="material-icons right">more_vert</i></span>
      <p><a href="#"></a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4">Asignar Cargo<i class="material-icons right">close</i></span>
      
  <div class="input-field col s12">
    <select>
      <option value="" disabled selected>Seleccione un Trabajador</option>
      <option value="1">Trabajador 1</option>
      <option value="2">Trabajador 2</option>
    </select>
    <label>Trabajadores</label>
  </div>

<div class="input-field col s12">
    <select>
      <option value="" disabled selected>Seleccione un Cargo</option>
      <option value="1">Departamental </option>
      <option value="2">Operacional </option>
    </select>
    <label>Cargos</label>
  </div>

    </div>
    </div>
    </div> <!-- Cerramos cargos -->

    <!-- Este sera el mantenedor de asignacion de actividades -->
    <div class="col s4">
    <div class="card small">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="images/office.png">
    </div>
    <div class="card-content">
      <span class="card-title activator grey-text text-darken-4">Actividades<i class="material-icons right">more_vert</i></span>
      <p><a href="#"></a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
      
        <div class="input-field col s12">
    <select>
      <option value="" disabled selected>Seleccione a un Trabajador</option>
      <option value="1">Trabajador 1</option>
      <option value="2">Trabajador 2</option>
    </select>
    <label>Trabajadores</label>
  </div>

        <div class="input-field col s12">
    <select>
      <option value="" disabled selected>Seleccione Actividad a Asignar</option>
      <option value="1">Actividad 1</option>
      <option value="2">Actividad 2</option>
    </select>
    <label>Actividades</label>
  </div>

    </div>
    </div>
    </div>
    </div> <!-- Cerramos actividades -->
    </div> <!-- Con este div cerramos la primera grid de opciones -->

    <!-- En este contenedor se mostraran diferentes listados (usuarios registrados, cargos y actividades segun su disponibilidad) -->
    <div class="row">
    <div class="col s12">
    <div class="card">
    <div class="card-content">
      <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
    </div>
    <div class="card-tabs">
      <ul class="tabs tabs-fixed-width">
        <li class="tab"><a class="active" href="#test4">Test 1</a></li>
        <li class="tab"><a href="#test5">Test 2</a></li>
      </ul>
    </div>

    <div class="card-content grey lighten-4">
      <!-- En la primera tab mostraremos los usuarios registrados -->
      <div id="test4">Test 1

      <!-- Tabla donde se cargan los datos -->
      <table class="striped">
      <thead>
          <tr>
              <th>Nombre Usuario</th>
              <th>Cargo</th>
              <th>Actividad</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Alvin</td>
            <td>Eclair</td>
            <td>$0.87</td>
          </tr>
          <tr>
            <td>Alan</td>
            <td>Jellybean</td>
            <td>$3.76</td>
          </tr>
          <tr>
            <td>Jonathan</td>
            <td>Lollipop</td>
            <td>$7.00</td>
          </tr>
        </tbody>
      </table>
      </div>

      <!-- En la segunda tab mostraremos las actividades -->
      <div id="test5">Test 2</div>

    </div>
  </div>

    </div>    
    </div>

<!-- Footer -->
  <footer class="page-footer">
    <div class="container">
    <div class="row">
    <div class="col l6 s12">
      <h5 class="white-text">Footer Content</h5>
        <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
    </div>
    <div class="col l4 offset-l2 s12">
        <h5 class="white-text">Links</h5>
        <ul>
          <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
  </footer>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>

      <!-- Con esto iniciamos el menu desplegable -->
      <script type="text/javascript">
        $(function(){
          $(".button-collapse").sideNav();
        });
      </script>

      <!-- Con esto inciamos el combobox -->
      <script type="text/javascript">
        $(function(){
            $(document).ready(function() {
            $('select').material_select();
          });
        });
      </script>

    </body>
  </html>
