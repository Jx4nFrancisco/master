<?php #error_reporting(E_ALL);ini_set('display_errors', 'On');
#header('Content-Type: application/json; charset=utf-8');
/*
* Metodo para iniciar sesion en el sistema para prueba que hereda metodos de
* EntidadesBaseCrud para validar inicio
*/

class RecuperarPassword extends ModeloBase
{
    private $table;
    private $column;
    private $database;
    public function __construct($table, $column)
    {
        # instancia conectar
        $dir = '_connection.php';
        $ruta_connect = PATH_COR.$dir;
        require_once $ruta_connect;
        /** @var Conectar [description] instancio Classe para generar coneccion a BD */
        $conectar = new Conectar();
        $coneccion = $conectar->conexion();
        /** Variables que almacenan los parametros recibidos */
        $this->table = (string)$this->table;
        $this->$column = (string)$this->column;
        $this->database = $coneccion;
        parent::__construct($column, $table, $coneccion);
    }# fin constructor
    /**
     * [recPass description]
     * Metodo que buscara mi usuario y traera sus datos segun lo que le pida
     * #param  [type] $where [description]
     * parametros que le paso al metodo
     * #return [type]        [description]
     * retorno de coincidencia segun lo pasado al metodo
     */
    public function recPass($where)
    {
        $resultSet = $this->getByWhere($where);
        return $resultSet;
    }
    /**
     * [verifyCode description]
     * Metodo que verificara que el codigo sea el correcto
     * #param  [type] $codigo  [description]
     * Codigo que le pasamos al metodo
     * #param  [type] $table   [description]
     * Tabla
     * #param  [type] $usuario   [description]
     * Usuario
     */
    public function verifyCode($codigo, $table, $usuario)
    {
        echo $codigo;
        echo $usuario;
        echo $table;
        $stmtCode = $this->databse->prepare("SELECT $column FROM $table WHERE tb1.codigo ");
    }
    /**
     * [numeroRandom description]
     * Metodo que ingresara mi codigo a mi base de datos
     * #param  [type] $codigo  [description]
     * Codigo que le pasamos al metodo
     * #param  [type] $table   [description]
     * Tabla
     * #param  [type] $usuario [description]
     * Usuario que se vera afectado
     */
    public function numeroRandom($codigo, $table, $usuario, $mail)
    {
        # echo $usuario;
        # echo $mail;
        # print_r("UPDATE $table SET tb1.codigo_validacion = '$codigo' WHERE tb1.usuario = ?");
        $stmtRandom = $this->database->prepare("UPDATE $table SET tb1.codigo_validacion = '$codigo' WHERE tb1.usuario = ?");
        $stmtRandom->bindParam(1, $usuario);
        $stmtRandom->execute();
        /** @var [description] variable que devuelve nuemero de columnas afectadas */
        $filaAfectada = $stmtRandom->rowCount();
        # echo $filaAfectada;
        /** Acontinuacion se borrara el codigo l cabo de un lapso de 10 minutos */
        if ($filaAfectada>0) {
            # echo 'filas afectada detectada';
            /**
             * Como un codigo para recuperar clave fue insertado correctamente
             * procedemos a enviarlo por mail al usuario
             * A su vez el codigo sera valido por dies minutos
             */
            $para = $mail;
            $titulo = 'no responder';
            $asunto = 'Hola:'. $usuario.'\r\nSu codigo para recuperar contraseña es :'.$codigo.'\r\n No responder\r\n Millahuin ingenieria y construccion Ltda';
            $mail=  mail($para, $titulo, $asunto);
            echo $asunto;
            echo 'hola';
        }
        # cierro la coneccion
        $stmtRandom = null;
        $this->database = null;
        return $stmtRandom;
    }
}# fin class
