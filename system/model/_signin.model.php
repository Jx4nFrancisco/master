<?php #error_reporting(E_ALL);ini_set('display_errors', 'On');
#header('Content-Type: application/json; charset=utf-8');
/*
* Metodo para iniciar sesion en el sistema para prueba que hereda metodos de
* EntidadesBaseCrud para validar inicio
*/
# construir ruta
#require_once '../../../configuration/_global.php';
# path
//$dir = 'modelo_base.php';
//$ruta_core = PATH_COR.$dir;
//require_once $ruta_core;

class SignIn extends ModeloBase
{
    private $table;
    private $column;
    # private $coneccion;
    public function __construct($table, $column)
    {
        # instancia conectar
        $dir = '_connection.php';
        $ruta_connect = PATH_COR.$dir;
        require_once $ruta_connect;

        $this->table = (string)$this->table;
        $this->$column = (string)$this->column;
        $conectar = new Conectar();
        $coneccion = $conectar->conexion();
        parent::__construct($column, $table, $coneccion);
    }# fin constructor

    public function signIn($where)
    {
        $resultSet = $this->getByWhere($where);
        return $resultSet;
    }
}# fin class
