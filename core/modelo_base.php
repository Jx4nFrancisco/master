<?php # error_reporting(E_ALL);ini_set('display_errors', 'On');
/**
* Este modelo base me contendra la ejecucion de consultas aun mas complejas
* y asu ves heredara las modelos de consultas de mi EntidadesBaseCrud
*/
#require_once '_entidades.php';

class ModeloBase extends EntidadesBaseCrud
{
    private $table;
    public function __construct($column, $table, $coneccion)
    {
        $this->table = (string) $table;
        parent::__construct($column, $table, $coneccion);
    }

    public function ejecutarSql($query)
    {
        # echo $query .'<br>';
        $stmtQuery = $this->database()->prepare($query);# query
        $stmtQuery -> execute();
        if ($stmtQuery == true) {
            $num_rows = $stmtQuery->rowCount();
            if ($num_rows > 1) {
                $i=0;
                while ($row = $stmtQuery->fetchAll(PDO::FETCH_OBJ)) {
                    $resultSet[] = $row ;
                    #$dato[] = $row[$i];
          #$i++;
                }
            } elseif ($num_rows == 1) {
                if ($row = $stmtQuery->fetchAll(PDO::FETCH_OBJ)) {
                    $resultSet = $row;
                }
            } else {
                $resulSet = true;
            }
        } else {
            $resultSet = false;
        }
        return $resultSet;
    }
}

/*
* Prueba de integracion de ModeloBase y EntidadesBaseCrud
* test de metodos EntidadesBaseCrud
* test de metodos ModeloBase
*/
#  require_once '_connection.php';
#  $column = 'nombre,apellido';
#  $table = 'tbl_personas';
#  $conectar = new Conectar();
#  $coneccion = $conectar->conexion();
#  $modelo_base = new ModeloBase($column,$table,$coneccion);

# Prueba metodo EntidadesBaseCrud
#  $data = array(1,2,'dario','espinoza',987654321);
#  $where = 'nombre = ? AND apellido = ? OR movil = ?';
#  $res1 = $modelo_base->getInsertTable($data,$where);
#  echo '<br><br>disable<br><br>';
 #print_r($res1);

 # Prueba metodo ejecutarSql
 # $id = 'Alan%';
 # $query = 'SELECT nombre, apellido FROM '.$table.' WHERE nombre like "'.$id.'"';
 # $res2 = $modelo_base->ejecutarSql($query);
 # print_r($res2);
