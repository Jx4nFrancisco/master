<?php #error_reporting(E_ALL);ini_set('display_errors', 'On');

# $dir = 'model/';

# $ruta = PATH_SYST_MODE;

/**
* Este controlador base contiene mi Class ControladorBase que heredaran
* todos mis controladores, tambien carga las clases de EntidadesBaseCrud
* y ModeloBase y todos los modelos creados en mi directorio model del sistema
*/
/*
class ControladorBase
{
    public function __construct()
    {
        require_once '_entidades.php';
        require_once '_hash.php';
        require_once 'modelo_base.php';


        # incluir todos los modelos
        foreach (glob($ruta.'*.model.php') as $files) {
            require_once $files;
        }
    }# fin constructor

    # Metodo que reenderiza mis vistas
    public function renderViews($vista, $dato)
    {
        # creo una variable dinamica al recibir datos como array desde el controlador
        print_r($dato);
        foreach ($dato as $id_obj => $value) {
            ${$id_obj}=$value;
        }
        # instancio mi renderisador de vistas
        # require_once PATH_COR.'metodos_vista.php';
        # require_once PATH_SYST.'views/'.$vista.'_views.html';
    }

  # Metodo de redireccion
  # public function redirect($controller='CONTROLADOR',$accion='ACCION') {
  #  header("Location:index.html?controller=".$controller."&action=".$accion);
  #}
}*/# fin class

require_once '../configuration/_global.php';

# variables para validar que directorio debo ingresar
$validacion = $_GET['variable'];
# formuarios signin
$usuario = $_POST["usuario"];
$contraseña = $_POST["contraseña"];
# formulario recuparar contraseña
$rec_pass = $_POST['recuperarUsuario'];

# require global para todos mis archivos .php
if (PATH_RAIZ) {
    /* Require mis archibos de directorio core */
    foreach (glob(PATH_COR.'*.php') as $filesCore) {
        require_once $filesCore;
        #print $filesCore.'<br>';
    }

    /* Require mis archibos de directorio system/model */
    # modelo signIn
    if ($usuario != '' && $contraseña != '') {
        foreach (glob(PATH_SYST_MODE.'_signin.model.php') as $filesModel) {
            require_once $filesModel;
            #print $filesModel.'<br>';
        }
        /* Require mis archibos de directorio system/controller */
        foreach (glob(PATH_SYST_CONT.'_ingreso/_signin.controller.php') as $filesController) {
            require_once $filesController;
            #print $filesController.'<br>';
        }
    }
    # model rec_pass
    if ($rec_pass != '') {
        foreach (glob(PATH_SYST_MODE.'_rec_pass.model.php') as $filesModel) {
            require_once $filesModel;
            #print $filesModel.'<br>';
        }
        /* Require mis archibos de directorio system/controller */
        foreach (glob(PATH_SYST_CONT.'_ingreso/_rec_pass.controller.php') as $filesController) {
            require_once $filesController;
            #print $filesController.'<br>';
        }
        /* Como estoy recuperando password tambien necesito funcion especial para enviar correo
         * y borrar codigo al cabo de diez minutos
         */
        /*foreach (glob(PATH_SYST.'function/_rec_pass.function.php') as $filesController) {
            require_once $filesController;
            #print $filesController.'<br>';
        }*/
    }

    /* Require mis archibos de directorio system/model/function
    if ($validacion == '_resetCode') {
    foreach (glob(PATH_SYST_MODE.'function/*.function.php') as $filesModelFunction) {
      require_once $filesModelFunction;
      #print $filesModelFunction.'<br>';
    }
    }
    */
}
