<?php #error_reporting(E_ALL);ini_set('display_errors', 'On');
class dataChange
{
    /* Crea un string codificado a partir de un array
    * no codificado algo como [3,1,2,"dario"]
    *@return json
    */
    public static function arrayEncode($array)
    {
        return base64_encode(json_encode(implode(',', $array)));
        #return json_encode(implode("[{':,'}]",$array));
    }
    /* Crea un array a partir de un string codificado
    * no codificado algo como Array ( [0] => 3 [1] => 1 [2] => 2 [3] => dario)
    * @return Array php
    */
    public static function arrayDecode($array_texto)
    {
        return json_decode((base64_decode($array_texto)));
    }
    /* Creo un array a partir de un string
    * ejemplo $string = 3','1','2','dario','espinoza','987654321
    * @return array
    */
    public static function arrayChange($dato)
    {
        return preg_split("/[,'=AND OR?]+/", $dato, -1, PREG_SPLIT_NO_EMPTY);
    }
    /* Creo un array a vidimencional de mi objeto retornado por ajax para leerlo con php
    * ejemplo Array
      (
          [0] => Array
              (
                  [usuario] => aflores
              )
      )
    * @return array
    */
    public static function arrayBidirecional($datos)
    {
        # print_r($datos);
        $resulSet = json_encode($datos);
        # print_r('hola'.$resulSet);
        $resulSet = json_decode($resulSet, true);
        # print_r($resulSet);
        return $resulSet;
    }
}
