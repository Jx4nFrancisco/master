<?php
/*-- Encritacion de mis claves y codigos de validacion --*/

/**
 * Este código evaluará el servidor para determinar el coste permitido.
 * Se establecerá el mayor coste posible sin disminuir demasiando la velocidad
 * del servidor. 8-10 es una buena referencia, y más es bueno si los servidores
 * son suficientemente rápidos. El código que sigue tiene como objetivo un tramo de
 * ≤ 50 milisegundos, que es una buena referencia para sistemas con registros interactivos.
 */
/*
$timeTarget = 0.05; // 50 milisegundos
$userPasswd = 'aflores';
$min = 11111999;
$max = 99999999;
$saltPasswd = random_int($min, $max);
echo $saltPasswd."<br>";
$coste = 8;
do {
    $coste++;
    $inicio = microtime(true);
    $passwd = password_hash($saltPasswd.$userPasswd, PASSWORD_DEFAULT, ["cost" => $coste]);
    $fin = microtime(true);
} while (($fin - $inicio) < $timeTarget);

#echo "Coste apropiado encontrado: " . $passwd . "\n";

$passwdVerify=password_verify($saltPasswd.$userPasswd, $passwd);
if ($passwdVerify) {
    echo '¡La contraseña es válida!';
} else {
    echo 'La contraseña no es válida.';
}*/
/**
 * [Encrypt description]
 * Esta clase se encarga de ejecutar metodos para la creacion de mis _hash
 */
class Encrypt
{
    private $random;
    public function __construct($random)
    {
        $this->random = $random;
    }
    /** [encriptarPass description] metodo para crear hash sobre nuestra passwd */
    public function encriptarPass($passwd)
    {
        # $this->passwd='aflores';
        $this->passwd = $passwd;
        # definimos variables para pasar mi clave a mi _hash
        $timeTarget = 0.05; // 50 milisegundos
        $userPasswd = $passwd; //password
        $coste = 8;
        do {
            $coste++;
            $inicio = microtime(true);
            $passwd=password_hash($userPasswd, PASSWORD_DEFAULT, ["cost" => $coste]);
            $fin = microtime(true);
        } while (($fin - $inicio) < $timeTarget);

        # echo "Coste apropiado encontrado: " . $passwd . "\n";

        /*$passwdVerify=password_verify($userPasswd, $passwd);
        if ($passwdVerify) {
            echo '¡La contraseña es válida!';
        } else {
            echo 'La contraseña no es válida.';
        }*/
    }

    /** [encriptarCode description]# metodo para crear hash sobre nuestro random */
    public function encriptarCode()
    {
        # generar mi code y luego pasar a mi _hash
        $timeTarget = 0.05; // 50 milisegundos
        // $min = 11111999; // random 8 digitos
        // $max = 99999999; // random 8 digitos
        // $numeroRandom = random_int($min, $max);
        $numeroRandom=$this->random;
        $coste = 8;
        do {
            $coste++;
            $inicio = microtime(true);
            $random = password_hash($numeroRandom, PASSWORD_DEFAULT, ["cost" => $coste]);
            $fin = microtime(true);
        } while (($fin - $inicio) < $timeTarget);
        # retornamo mi numero encriptado
        # print_r($numeroRandom);
        return $random;
    }
    /** [encriptarCode description]# metodo para crear hashverify sobre nuestro random */
    public function desencriptarCode()
    {
        # generar mi code y luego pasar a mi _hash
        $timeTarget = 0.05; // 50 milisegundos
        // $min = 11111999; // random 8 digitos
        // $max = 99999999; // random 8 digitos
        // $numeroRandom = random_int($min, $max);
        $numeroRandom=$this->random;
        $coste = 8;
        do {
            $coste++;
            $inicio = microtime(true);
            $random = password_verify($numeroRandom, PASSWORD_DEFAULT, ["cost" => $coste]);
            $fin = microtime(true);
        } while (($fin - $inicio) < $timeTarget);
        # retornamo mi numero encriptado
        # print_r($numeroRandom);
        return $random;
    }
}
