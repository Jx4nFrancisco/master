<?php #error_reporting(E_ALL);ini_set('display_errors', 'On');
/**
 * [EntidadesBaseCrud description]
 * Esta clase me contendra metodos que heredaran mis modelos y que podran usar
 * para interactuar con la base de datos
 */
class EntidadesBaseCrud extends dataChange
{
    private $database;
    private $column ;
    private $table;
    private $conectar;

    public function __construct($column, $table, $coneccion)
    {
        # Metodo Le paso valor a mi variables
        $this->column = (string) $column;
        $this->table = (string) $table;
        $this->database = $coneccion;
        $this->conectar = null;
    }

    # Metodo funciones publicas para tareas a la base de datos
    public function getDesconectar()
    {
        $this->database = null;
        return $this->conectar = $this->database;
    }

    public function dataBase()
    {
        return $this->database;
    }

    # Metodo buscar todo en una tabla por id
    public function getSelectByAll()
    {
        $stmtByAll = $this->database->prepare("SELECT * FROM $this->table ORDER BY id ASC");# query
        $stmtByAll->execute();

        while ($row = $stmtByAll->fetch(PDO::FETCH_OBJ)) {
            #$resultSet['data'][]=$row;
            $resultSet[]=$row;
        }
        #cierro la conneccion
        $stmtByAll = null;
        $this->database=null;
        #print_r($this->database->errorInfo());
        return $resultSet;
    }

    # Metodo buscar por id y mostrar las columnas que se pidan
    public function getById($id)
    {
        # prueba para imprimir consulta
        # print_r("SELECT $this->column FROM $this->table WHERE id=$id");
        $stmtId = $this->database->prepare("SELECT $this->column FROM $this->table WHERE id = ?");# query
        $stmtId->bindParam(1, $id);
        $stmtId->execute();

        if ($row = $stmtId->fetch(PDO::FETCH_OBJ)) {
            $resultSet=$row;
        }
        #cierro la conneccion
        $stmtId = null;
        $this->database=null;
        # print_r($this->database->errorInfo());
        return $resultSet;
    }

    # Metodo buscar por columnas que se pidan y por valor de columna que se solicite en where
    public function getBy($column, $value)
    {
        # prueba para imprimir consulta
        # print_r("SELECT $this->column FROM $this->table WHERE $column='$value'");
        $stmtBy = $this->database->prepare("SELECT $this->column FROM $this->table WHERE $column = ?");# query
        $stmtBy->bindParam(1, $value);
        $stmtBy->execute();

        while ($row = $stmtBy->fetch(PDO::FETCH_OBJ)) {
            $resultSet[]=$row;
        }
        #cierro la conneccion
        $stmtBy = null;
        $this->database=null;
        # print_r($this->database->errorInfo());
        return $resultSet;
    }

    # Metodo buscar por columnas que se pidan y por valor de columna que coincidad en where campo like
    public function getByLike($column, $value)
    {
        # prueba para imprimir consulta
        # print_r("SELECT $this->column FROM $this->table WHERE $column='$value'");
        $stmtByLike = $this->database->prepare("SELECT $this->column FROM $this->table WHERE $column ?");# query
        $stmtByLike->bindParam(1, $value);
        $stmtByLike->execute();

        while ($row = $stmtByLike->fetch(PDO::FETCH_OBJ)) {
            $resultSet[]=$row;
        }
        #cierro la conneccion
        $stmtByLike = null;
        $this->database=null;
        # print_r($this->database->errorInfo());
        return $resultSet;
    }

    # Metodo buscar por columnas que se pidan y por valor de columnas que se solicite en where
    public function getByWhere($where)
    {
        # prueba para imprimir consulta
        # print_r("SELECT $this->column FROM $this->table $where");
        $stmtWhere = $this->database->prepare("SELECT $this->column FROM $this->table $where");# query
        #$stmtWhere->bindParam($where);
        $stmtWhere->execute();
        while ($row = $stmtWhere->fetchAll(PDO::FETCH_OBJ)) {
            $resultSet = $row;
        }
        #cierro la conneccion
        #$stmtWhere = null;
        #$this->database=null;
        #print_r($this->database->errorInfo());
        return $resultSet;
    }

    # Metodo buscar anidado
    public function getByAnidacion($anidacion, $column, $value)
    {
        $stmtAnidacion=$this->database->prepare("SELECT $this->column FROM $this->table WHERE $anidacion $column = ?");# query
        $stmtAnidacion->bindParam(1, $value);
        $stmtAnidacion->execute();

        while ($row = $stmtAnidacion->fetch(PDO::FETCH_OBJ)) {
            $resultSet[]=$row;
        }
        #cierro la conneccion
        $stmtAnidacion = null;
        $this->database=null;
        # print_r($this->database->errorInfo());
        return $resultSet;
    }

    # Metodo dar update recursos de base de dato
    public function getUpdateId($id, $column, $value)
    {
        $stmtUpdate=$this->database->prepare("UPDATE $this->table SET $column='$value' WHERE id = ?");# query
        $stmtUpdate->bindParam(1, $id);
        $stmtUpdate->execute();
        #cierro la conneccion
        $stmtUpdate = null;
        $this->database=null;
        # print_r($this->database->errorInfo());
        return $stmtUpdate;
    }

    # Metodo insertar datos a una tabla
    public function getInsertTable($data, $where)
    {

        /*
        * Pasamos el array a json y luego php pero con formato de string
        * asi la insercion de los datos no tendra problemas de validacion
        */
        $data=$this->arrayEncode($data);
        $data=$this->arrayDecode($data);

        # prueba para imprimir mi insert y ver si estan correctos los datos
        #print_r("INSERT INTO $this->table($this->column) VALUES('$data')");
        #print_r("SELECT $this->column FROM $this->table WHERE $where");

        /*
        * Validar que no existan datos duplicados
        * convertimos mi string $data en array para validar
        * lo mismo con variable column
        * preguntamos si exiten los datos en la BD
        * para ello uso el array con los nombre de las columnas y las agrego al where de mi select segun dato que no pueda repetirse
        */
        $arrayData = $this->arrayChange($data);
        $nombreColumna = $this->arrayChange($this->column);
        $whereArray = $this->arrayChange($where);
        # query
        $stmtExiste = $this->database->prepare("SELECT $this->column FROM $this->table WHERE $where");
        # echo "SELECT $this->column FROM $this->table WHERE $where <br>";
        # print_r($whereArray); # echo '<br>'; # print_r($nombreColumna);
        /*
        * Recorro mi nombreColumna para dejar fuera las variable que no importa comparar
        * lo mismo hago con mi arrayData
        */
        # guardo mi array column completo
        $allColumn = $nombreColumna;
        $j=0;//numero que indica la psicion en el array de referencia
        for ($i=1; $i <= count($nombreColumna); $i++) {
            # dejo fuera las columnas id*
            if (strncasecmp($nombreColumna[$j], 'id_', 3) != true) {
                #elimino mi coincidencia de mi array
                unset($nombreColumna[$j]);
            }
            $j++;
        }
        # print_r($allColumn); echo '<br>'; print_r($nombreColumna);
        $n=2;//numero que indica la posicion en el arrayData de referencia
        $value=1;//value que corresponde a posicion de signo ? en el where
        for ($i=1; $i <= count($whereArray); $i++) {
            $variable = $arrayData[$n];
            if ($i <= count($whereArray)) {
                $stmtExiste->bindValue($value, $variable, PDO::PARAM_STR);
                $stmtExiste->execute();
                $resultado = $stmtExiste->fetch(PDO::FETCH_ASSOC);
                if ($resultado != 0) {
                    # $resultado devolvió la fila
                    #echo '<br>El dato : '.$variable. ' ya se encuentra y no puede existir duplicidad de este dato';
                    echo '<br>';
                } elseif ($resultado == 0) {
                    # si el dato no existe lo almaceno
                    #echo '<br>El dato : '.$variable. ' no existe para agregar modifique el dato que exite<br>';
                }
            }
            $n++;
            $value++;
        }
    }
    /*
    # eliminar datos a una tabla
    public function getDeleteDatoId($id){
        $stmtDelete=$this->database->prepare("DELETE FROM $this->table WHERE id = ?");
        $stmtDelete->bindParam(1,$id);
        $stmtDelete->execute();
        #cierro la conneccion
        $stmtDelete = null;
        $this->database=null;
        return $stmtDelete;
    }
    */
}
#Prueba de sentencia a base de dat
/*
$column='id,id_tipo_empleado,id_estado_persona,nombre,apellido
,movil';
$table='tbl_personas';
$bridge='';
$result= new EntidadesBaseCrud($column, $table, $bridge);
*/
/*
$res=$result->getById('1');
$res2=$result->getSelectByAll();
$column='nombre_empresa';
$value = 'Dimasur Ltda';
$res3=$result->getBy($column,$value);
$id=1;
$column='descripcion';
$value='cambiado por update';
$res4=$result->getUpdateId($id,$column,$value);
echo 'getSelectByAll<br><br>';
print_r(json_encode($res2));
echo '<br><br>getById<br><br>';
print_r(json_encode($res));
echo '<br><br>getBy<br><br>';
print_r(json_encode($res3));
echo '<br><br>disable<br><br>';
print_r(json_encode($res4));
echo '<br><br>insert<br><br>';
*/
#prueba insert
/*
$table='tbl_personas';
$bridge='';
$data=array(1,2,'dario','espinoza',987654321);
$where='id_tipo_empleado = ? OR id_estado_persona = ? OR  nombre = ? AND apellido = ? OR movil = ?';
$res5=$result->getInsertTable($data,$where);
echo '<br><br>disable<br><br>';
print_r($res5);
*/
/*
$res6=$result->getDeleteDatoId('12');
echo '<br><br>disable<br><br>';
print_r($res6);
*//*
$column='nombre_empresa like';
$value = 'D%';
$res7=$result->getByLike($column,$value);
print_r(json_encode($res7));
*/
