<?php #error_reporting(E_ALL);ini_set('display_errors', 'On');
///clase que devolvera ruta de directorios
class RutaPadre
{
    protected $ruta_dir;
    protected $dir;
    protected $ruta_principal;
    protected $ruta_config ;
    protected $ruta_component;
    protected $ruta_system;

    public function __construct($dir)
    {
        $this->ruta_dir = dirname(__file__, 2);
        $this->dir=$dir;
        //objetos de rutas
        $this->ruta_principal=$this->set_dirname_raiz();
        $this->ruta_config=$this->set_dirname_config();
        $this->ruta_component=$this->set_dirname_comp();
        $this->ruta_system=$this->set_dirname_system();
    }
    /*public function set_dirname_raizP()
    {
      //variable con ruta de directorio
      $ruta = $this->ruta_dir;
      //array con string de los directorio
      //$ruta = explode('/',$ruta);
      //var con nombre de directorios
      $raiz = $this->ruta_dir.$this->dir;

      return $raiz;
    }*/
    /*
    * Ruta absoluta
    */
    public function set_dirname_raiz()
    {
        //variable con ruta de directorio
        $ruta = $this->ruta_dir;
        //array con string de los directorio
        //$ruta = explode('/',$ruta);
        //var con nombre de directorios
        $raiz = $this->ruta_dir.$this->dir;

        return $raiz;
    }
    /*
    * Ruta absoluta Directorio config...
    */
    public function set_dirname_config()
    {
        //variable con ruta de directorio
        $ruta = $this->ruta_dir;
        //array con string de los directorio
        //$ruta = explode('/',$ruta);
        //var con nombre de directorios
        $config = $this->ruta_dir.'/configuration'.$this->dir;

        return $config;
    }
    /*
    * Ruta absoluta Directorio syst...
    */
    public function set_dirname_system()
    {
        //variable con ruta de directorio
        $ruta = $this->ruta_dir;
        //array con string de los directorio
        //$ruta = explode('/',$ruta);
        //var con nombre de directorios
        $system = $this->ruta_dir.'/system'.$this->dir;

        return $system;
    }
    /*
    * Ruta absoluta Directorio compo...
    */
    public function set_dirname_comp()
    {
        //variable con ruta de directorio
        $ruta = $this->ruta_dir;
        //array con string de los directorio
        //$ruta = explode('/',$ruta);
        //var con nombre de directorios
        $components = $this->ruta_dir.'/components'.$this->dir;

        return $components;
    }
}

class RutaAbsoluta extends RutaPadre
{
    protected $dir;
    public function __construct($dir)
    {
        parent::__construct($dir);
    }
    public function __toString()
    {
        return $this->ruta_principal;
    }
}
class RutaComponents extends RutaPadre
{
    protected $dir;
    public function __construct($dir)
    {
        parent::__construct($dir);
    }
    public function __toString()
    {
        return $this->ruta_component;
    }
}
class RutaSystem extends RutaPadre
{
    protected $dir;
    public function __construct($dir)
    {
        parent::__construct($dir);
    }
    public function __toString()
    {
        return $this->ruta_system;
    }
}
class RutaConfig extends RutaPadre
{
    protected $dir;
    public function __construct($dir)
    {
        parent::__construct($dir);
    }
    public function __toString()
    {
        return $this->ruta_config;
    }
}

/*
//ejemplo para posicionarme en mis directoriospara ayudar el ingreso de rutas
//dir Gestion_Millahuin
$dir= '';# directorio
$ruta = new RutaPadre($dir);# instancia de ruta
$ruta->set_dirname_raiz();# guardamos ruta devuelta
print $ruta .'</br>';# visualiso ruta
//dir components
$ruta = new RutaComponents($dir);
$ruta->set_dirname_comp();
print $ruta.'</br>';
//dir system
$ruta = new RutaSystem($dir);
$ruta->set_dirname_system();
print $ruta.'</br>';
//dir configuration
$ruta = new RutaConfig($dir);
$ruta->set_dirname_config();
print $ruta.'</br>';
*/
