<?php #error_reporting(E_ALL);ini_set('display_errors', 'On');
class Conectar
{
    private $driver;
    private $host;
    private $user;
    private $pass;
    private $db;
    private $charset;

    public function __construct()
    {
        # require_once '../../../configuration/_global.php';
        //dir configuration
        $dir ='._database.php';
        $ruta = PATH_CONFIG.$dir;
        $db_cfg = require_once $ruta;
        $this->driver=$db_cfg["driver"];
        $this->host=$db_cfg["host"];
        $this->user=$db_cfg["user"];
        $this->pass=$db_cfg["pass"];
        $this->db=$db_cfg["db"];
        $this->charset=$db_cfg["charset"];
    }

    public function conexion()
    {
        try {
            if ($this->driver=="mysql" || $this->driver==null) {
                $con = new PDO($this->driver.':host='.$this->host.';dbname='.$this->db, $this->user, $this->pass);
                $con->query("SET NAMES '".$this->charset."'");
            }

            return $con;
        } catch (PDOException $e) {
            echo 'Falló la conexión: ' . $e->getMessage();
            die();
        }
    }
}
/*
//ejemplo de test coneccion
$conect = new Conectar();
$conex = $conect->conexion();
$query = $conex->query("select * from tbl_personas");
$query = $query->fetchALL(PDO::FETCH_ASSOC);
echo '<pre>',print_r(($query)),'</pre>';
*/
