<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Millahuin ltda</title>
  <!-- favicon -->
  <link href="millahuin_transp.ico" rel="shortcut icon" type="image/x-icon" />
  <!--link-->
  <link rel="stylesheet" href="components/framework/materialize/css/materialize.min.css" >
  <link rel="stylesheet" href="components/libs/fontawesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="system/views/style/css/style.min.css">
</head>

<body dataType="inicio">
  <span id="respuesta"></span>
  <header>
    <!--header-->
    <!--/header-->
  </header>
  <main>
    <!--main-->
    <!--/main-->
  </main>
  <footer class="page-footer position-bottom">
    <!--footer-->
    <!--/footer-->
  </footer>
  <!--Script-->
  <script type="text/javascript" src="components/libs/jquery.min.js" ></script>
  <script type="text/javascript" src="components/framework/materialize/js/materialize.min.js"></script>
  <script type="text/javascript" src="system/views/js/load.js/loadasync.min.js"></script>
</body>

</html>
<?php error_reporting(0);
require_once './core/controlador_base.php'; ?>
